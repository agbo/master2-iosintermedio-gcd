//
//  KCGImageControllerViewController.h
//  GCD
//
//  Created by Fernando Rodríguez Romero on 11/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KCGImageControllerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
- (IBAction)changeAlpha:(id)sender;
- (IBAction)syncDownload:(id)sender;
- (IBAction)asyncDownload:(id)sender;
- (IBAction)fancyAsyncDownload:(id)sender;

@end
