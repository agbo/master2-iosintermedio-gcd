//
//  KCGImageControllerViewController.m
//  GCD
//
//  Created by Fernando Rodríguez Romero on 11/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import "KCGImageControllerViewController.h"

@interface KCGImageControllerViewController ()

@end

@implementation KCGImageControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)changeAlpha:(id)sender {
    
    UISlider *slider = (UISlider*) sender;
    
    self.photoView.alpha = slider.value;
}

- (IBAction)syncDownload:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"https://asiloveoscar.files.wordpress.com/2011/01/chepe-fortuna-venezuela1.jpg"];
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    UIImage *img = [UIImage imageWithData:data];
    
    self.photoView.image = img;

 
}

- (IBAction)asyncDownload:(id)sender {
    
    // Creamos una cola
    dispatch_queue_t download = dispatch_queue_create("falete bronceado", 0);
    
    // Le mandamos un bloque
    dispatch_async(download, ^{
        
        NSURL *url = [NSURL URLWithString:@"https://cdn.playbuzz.com/cdn/47169acc-3907-48cb-88fb-c13f98a556ad/90521427-6297-4109-ab50-c985316d2ad8.jpg"];
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        UIImage *img = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.photoView.image = img;
        });
        
        
    });

}

- (IBAction)fancyAsyncDownload:(id)sender {
    
    [self withImage:^(UIImage *img) {
        self.photoView.image = img;
    }];
}


-(void) withImage:(void (^)(UIImage*img))completionBlock{
    
    // Creamos la url
    NSURL *url = [NSURL URLWithString:@"https://s-media-cache-ak0.pinimg.com/originals/36/bc/bc/36bcbcb77ca4635ebcddd80596e677c0.jpg"];
    
    // Descargamos
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0),^{
        
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        UIImage *img = [UIImage imageWithData:data];

        // llamamos al bloque de finalizacion
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(img);
        });
    });
    
    
}













@end
