//
//  AppDelegate.h
//  GCD
//
//  Created by Fernando Rodríguez Romero on 11/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

